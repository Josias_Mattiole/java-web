package aplicacao;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Josias
 */
@Named(value = "clienteMB")
@ViewScoped
public class clienteMB {
    
    private String tipoPessoa = "J";
    private String estado = "SC";
    
    public clienteMB() {
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public boolean isPessoaFisica() {
        return "F".equals(tipoPessoa);
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<SelectItem> getCidades() {
        List<SelectItem> cidades = new ArrayList<>();
        if("SC".equals(estado)){
            SelectItem  cidade = new SelectItem("CRI", "Criciúma");
            cidades.add(cidade);
            cidades.add(new SelectItem("ARA", "Araranguá"));
            cidades.add(new SelectItem("URU", "Urussanga"));
        }else if("RS".equals(estado)){
                  SelectItem  cidade = new SelectItem("VIA", "Viamão");
                  cidades.add(cidade);
                  cidades.add(new SelectItem("POA", "Porto Alegre"));
                  cidades.add(new SelectItem("EST", "Esteio"));
        }else if("SP".equals(estado)){
                  SelectItem  cidade = new SelectItem("SAO", "São Paulo");
                  cidades.add(cidade);
                  cidades.add(new SelectItem("GRJ", "Guarujá"));
                  cidades.add(new SelectItem("SAN", "Santos"));
        }
        return cidades;
    }
}
