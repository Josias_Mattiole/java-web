package aplicacao;

import java.util.Random;
import javax.enterprise.context.Dependent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Josias Mattiole
 */
@Named(value = "adivinharNumeroMB")
@ViewScoped
public class AdivinharNumeroMB {

    private Integer numero;
    private Integer numeroRandon;
    private String msg;

    public AdivinharNumeroMB() {
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getNumeroRandon() {
        return numeroRandon;
    }

    public void setNumeroRandon(Integer numeroRandon) {
        this.numeroRandon = numeroRandon;
    }

    public void geraNumero() {
        GeraNumeroRandon numRand = new GeraNumeroRandon();
        numRand.geraNumero();
        numeroRandon = numRand.getNumeroRandon();
    }

    public void verificar() {

        System.out.println("Número asass: " + numero);

        System.out.println("Número Randon : " + numeroRandon);
        if (numeroRandon != null) {
            if (numero > 100) {
                msg = "O número digitado deve ser entre 0 e 100.";
            } else if (numero < numeroRandon) {
                msg = "O número digitado é menor que o número oculto.";
            } else if (numero > numeroRandon) {
                msg = "O número digitado é maior que o número oculto.";
            }else{
                msg = "Parabéns, você descobriu que o número oculto é:  " + numeroRandon;
            }
        }else{
            msg = "O Randon falhou!!";
        }
    }
}
