package aplicacao;

import java.util.Random;

/**
 *
 * @author Josias
 */
public class GeraNumeroRandon {
    
    private Integer numeroRandon;

    public Integer getNumeroRandon() {
        return numeroRandon;
    }

    public void setNumeroRandon(Integer numeroRandon) {
        this.numeroRandon = numeroRandon;
    }
    
    public Integer geraNumero(){
        Random gerador = new Random();
        numeroRandon = gerador.nextInt(100);
        return numeroRandon;
    }
    
}
