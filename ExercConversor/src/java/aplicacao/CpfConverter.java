package aplicacao;

import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.format.Formatter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Josias
 */
@FacesConverter("cpfConverter")
public class CpfConverter implements Converter{
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value){
        String cpfFormatado = null;
        Formatter formatador = new CPFFormatter();
        
        try{
            cpfFormatado = formatador.format(value);
        }catch(ConverterException ex){
             throw new ConverterException("Valor inválido para conversão.");
        }
        return cpfFormatado;
    }
    
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
        String cpfFormatado = value.toString();
        
        return cpfFormatado;
    }
}
