package aplicacao;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Josias
 */
@ManagedBean
@ViewScoped
public class ConversorMB {

    private String cpfNaoFormatado;
    private String cnpjNaoFormatado;
    
    public String getCpfNaoFormatado() {
        return cpfNaoFormatado;
    }

    public void setCpfNaoFormatado(String cpfNaoFormatado) {
        this.cpfNaoFormatado = cpfNaoFormatado;
    }

    public String getCnpjNaoFormatado() {
        return cnpjNaoFormatado;
    }

    public void setCnpjNaoFormatado(String cnpjNaoFormatado) {
        this.cnpjNaoFormatado = cnpjNaoFormatado;
    }
}
