package aplicacao;

import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.format.Formatter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Josias
 */
@FacesConverter("cnpjConverter")
public class CnpjConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value){
        String cnpj = null;
        Formatter formatador = new CNPJFormatter();
        
        try{
            cnpj = formatador.format(value);
        }catch(ConverterException ex){
            throw new ConverterException("Valor inválido para conversão.");
        }
        return cnpj;
    }
    
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object value){
        String cnpj = value.toString();
        
        return cnpj;
    }
}
