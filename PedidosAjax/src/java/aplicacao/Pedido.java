package aplicacao;

import java.util.*;

/**
 *
 * @author Josias
 */
public class Pedido {
    private String cliente;
    private String endereco;
    private List<Item> listaItens = new ArrayList<>();

    public Pedido(){
        
    }
    
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public List<Item> getListaItens() {
        return listaItens;
    }

    public void setListaItens(List<Item> listaItens) {
        this.listaItens = listaItens;
    }
    
    public Double getTotal(){
        Double total = 0.0;
        for(Item i : listaItens){
            total += i.getTotalItem();
        }
        return total; 
   }
}
