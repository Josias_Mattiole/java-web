package aplicacao;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Josias
 */
@ManagedBean
@ViewScoped
public class PedidoMB {
    private List<Pedido> listaPedidos = new ArrayList<>();
    private Pedido pedido = new Pedido();
    private Item item;

    public PedidoMB(){
        novoItem();
        novoPedido();
    }
    
    public void adicionaItem(){
        pedido.getListaItens().add(item);
        pedido.getTotal();
        novoItem();
    }
    
    public void adicionaPedido(){
        pedido.getListaItens().add(item);
        listaPedidos.add(pedido);
    }
    
    public void novoItem(){
        item = new Item();
    }
    
    public void novoPedido(){
        pedido = new Pedido();
    }
    
    public List<Pedido> getListaPedidos() {
        return listaPedidos;
    }

    public void setListaPedidos(List<Pedido> listaPedidos) {
        this.listaPedidos = listaPedidos;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
