package aplicacao;

/**
 *
 * @author Josias
 */
public class Item {
    private String produto;
    private Double quantidade = 0.0;
    private Double preco = 0.0;

    public Item(){
        
    }
    
    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
    
    public Double getTotalItem(){
        return quantidade * preco;
    }
}
