package aplicacao;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Josias
 */
@ManagedBean
@ViewScoped
public class TarefaMB {

    private Tarefa tarefa;
    private List<Tarefa> lista;
    
    public TarefaMB(){
        novo();
        atualizarLista();
    }
    
    public void novo(){
        tarefa = new Tarefa();
    }
    
    public void atualizarLista() {
        lista = TarefaRepositorio.getTarefas();
    }
    
    public void salvar(){
        TarefaRepositorio.salvar(tarefa);
        atualizarLista();
    }
    
    public void excluir(){
        excluir(this.tarefa);
    }
    
    public void excluir(Tarefa tarefa){
        TarefaRepositorio.excluir(tarefa);
        atualizarLista();
        novo();
    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa) {
        this.tarefa = tarefa;
    }

    public List<Tarefa> getLista() {
        return lista;
    }

    public void setLista(List<Tarefa> lista) {
        this.lista = lista;
    }
    
    
}
