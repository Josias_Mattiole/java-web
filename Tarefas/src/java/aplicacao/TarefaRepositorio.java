package aplicacao;

import java.util.List;
import javax.persistence.*;
/**
 *
 * @author Josias
 */
public class TarefaRepositorio {
    
    public static void salvar(Tarefa tarefa){
        EntityManager em = JPA.getEM();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.merge(tarefa);
        t.commit();
    }
    
    public static void excluir(Tarefa tarefa) {
        EntityManager em = JPA.getEM();
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.remove(em.find(Tarefa.class, tarefa.getCodigo()));
        t.commit();
    }
    
    public static Tarefa getTarefa(Integer codigo) {
        EntityManager em = JPA.getEM();
        return em.find(Tarefa.class, codigo);
    }
    
    public static List<Tarefa> getTarefas() {
        EntityManager em = JPA.getEM();
        return em.createQuery("select t from Tarefa t", Tarefa.class).getResultList();
    }
    
    public static List<Tarefa> getTarefas(String descricao) {
        EntityManager em = JPA.getEM();
        TypedQuery<Tarefa> query = em.createQuery("select t from Tarefa t where t.descricao "
                                                  + "like :descricao", Tarefa.class);
        query.setParameter("valor", "%" + descricao + "%");
        return query.getResultList();
    }
}
