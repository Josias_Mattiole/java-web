package aplicacao;

import javax.persistence.*;

/**
 *
 * @author Josias
 */
public class JPA {
    public static EntityManagerFactory emf;
    
    public static EntityManager getEM() {
        if(emf == null) {
            emf = Persistence.createEntityManagerFactory("TarefaJPAPU");
        }
        
        return emf.createEntityManager();
    }
}
